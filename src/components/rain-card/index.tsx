import React from 'react';

type RainCardProps = {
	willRain: boolean;
};

const RainCard = ({ willRain }: RainCardProps) => {
	return (
		<div>
			<h1>Is it going to rain?</h1>
			<p>{willRain ? 'Yes' : 'No'}</p>
		</div>
	);
};

export { RainCard };
