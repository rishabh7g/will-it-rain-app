import React from 'react';
import { render, screen } from '@testing-library/react';
import { RainCard } from '.';

describe('weather card', () => {
	it('display weather card', () => {
		const headingText = 'Is it going to rain?';
		const contentText = 'Yes';
		const { debug } = render(<RainCard willRain={true} />);

		const heading = screen.getByRole('heading');
		expect(heading).toHaveTextContent(headingText);

		const content = screen.getByText(contentText);
		expect(content).toBeInTheDocument();
	});
});
