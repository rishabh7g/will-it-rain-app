import React, { useEffect, useState } from 'react';
import OpenWeatherMeta from 'constants/openweather-meta.json';
import axios from 'axios';
import { RainCard } from 'components/rain-card';

type RainAppProps = {
	city: string;
};

const RainApp = ({ city }: RainAppProps) => {
	const [willItRain, setWillItRain] = useState(false);
	const [loading, setLoading] = useState(false);
	useEffect(() => {
		const fetchWeatherData = async () => {
			const url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${OpenWeatherMeta.apiKey}`;
			const result = await axios.get(url);
			setWillItRain(result.data.main.humidity > 50);
			setLoading(true);
		};
		fetchWeatherData();
	}, [city]);

	return <>{loading ? <RainCard willRain={willItRain} /> : null}</>;
};

export { RainApp };
