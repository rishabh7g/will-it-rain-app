import { RainApp } from 'pages/rain-app';
import React from 'react';
import './App.css';

function App() {
	return (
		<div className='App'>
			<RainApp city='London' />
		</div>
	);
}

export default App;
