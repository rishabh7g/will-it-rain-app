import 'whatwg-fetch';
import { act, render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { RainApp } from '.';

const server = setupServer(
	rest.get('http://api.openweathermap.org/data/2.5/weather', (req, res, ctx) =>
		res(ctx.status(200), ctx.json({ main: { humidity: 51 } }))
	)
);

beforeAll(() => server.listen());
afterAll(() => server.close());
afterEach(() => server.resetHandlers());

describe('Rain app page', () => {
	it('test rain app page ', async () => {
		render(<RainApp city='london' />);
		const headingText = 'Is it going to rain?';
		const contentText = 'Yes';

		expect(await screen.findByRole('heading')).toBeInTheDocument();
		const heading = screen.getByRole('heading');
		expect(heading).toHaveTextContent(headingText);

		const content = screen.getByText(contentText);
		expect(content).toBeInTheDocument();
	});
});
